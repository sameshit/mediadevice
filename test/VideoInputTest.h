#ifndef VideoInputTest______H
#define VideoInputTest______H

#include "../src/MediaDevice.h"

namespace MediaDeviceLib
{
	class VideoInputTest
	{
	public:
		VideoInputTest(CoreObjectLib::CoreObject *core);
		virtual ~VideoInputTest();

		bool Run();
	private:
		CoreObjectLib::CoreObject *_core;
		VideoInputManager *_video_manager;
		int _num_frame;

		void ProcessFrame(const LiveFormatLib::Frame &frame);
	};
}

#endif