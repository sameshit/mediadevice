#include "VideoInputTest.h"
#include "AudioInputTest.h"

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace std;

int main()
{
	setlocale(LC_ALL,"");
	CoreObject *_core = new CoreObject;
//	VideoInputTest *test;
	AudioInputTest *test;

	fast_new(test,_core);
	ABORT_IF_FALSE(test->Run());
	cin.get();
	fast_delete(test);

	delete _core;
	return 0;
}