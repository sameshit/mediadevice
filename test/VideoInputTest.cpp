#include "VideoInputTest.h"

using namespace CoreObjectLib;
using namespace MediaDeviceLib;
using namespace LiveFormatLib;
using namespace std;

VideoInputTest::VideoInputTest(CoreObject *core)
	:_core(core),_num_frame(0)
{
	fast_new(_video_manager,_core);
}

VideoInputTest::~VideoInputTest()
{
	fast_delete(_video_manager);
}

bool VideoInputTest::Run()
{
	list<VideoInputInfo> devices;
	VideoInputInfo info;
	VideoInput *input;
	VideoInputFormatList format_list;

	RETURN_IF_FALSE(_video_manager->Enum(&devices));

	cout <<"Input video device list:"<<endl;
	for (auto it = devices.begin(); it != devices.end(); ++it)
		cout << (*it).GetName() <<endl;
	cout << endl;

	if (!devices.empty())
	{
		info = (*devices.begin());
		RETURN_IF_FALSE(_video_manager->Create(&input,info));
		input->GetFrameEvent()->Attach(this,&VideoInputTest::ProcessFrame);
		RETURN_IF_FALSE(input->EnumFormats(&format_list));
		cout <<"Enum format list: "<<endl;
		for (auto it = format_list.begin(); it != format_list.end(); ++it)
			cout <<"width: "<<(*it).width<<", height: "<< (*it).height<<", max_fps: "
			<<(*it).max_fps.AsUint32()<<", min frame rate: "<<(*it).min_fps.AsUint32() << endl;

		auto it = format_list.begin();
		if (!format_list.empty())
			RETURN_IF_FALSE(input->Open((*it),Rational(35,1)));
		Utils::Delay(2*1000); // capture some frames
		LOG_INFO("Num frames: "<<_num_frame);
		RETURN_IF_FALSE(_video_manager->Destroy(&input));
	}
	return true;
}

void VideoInputTest::ProcessFrame(const Frame &frame)
{
	uint8_t *data;

	LOG_INFO("received frame with pts: "<<frame.pts<<", num frame: "<<++_num_frame);
}