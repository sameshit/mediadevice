#include "AudioInputTest.h"

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

AudioInputTest::AudioInputTest(CoreObject *core)
	:_core(core)
{
	fast_new(_manager,_core);
}

AudioInputTest::~AudioInputTest()
{
	fast_delete(_manager);
}

bool AudioInputTest::Run()
{
	AudioInputInfoList audio_list;
	AudioInput *input;
	AudioInputFormatList format_list;

	RETURN_IF_FALSE(_manager->Enum(&audio_list));

	cout << "Audio input list: "<<endl;
	for (auto it = audio_list.begin();
		it != audio_list.end();
		++it)
	{
		cout<<(*it).GetName() <<" ";
	}

	RETURN_IF_FALSE(_manager->Create(&input,(*audio_list.begin())));
	RETURN_IF_FALSE(input->EnumFormats(&format_list));

	cout <<"Formats: "<<endl;
	for (auto it = format_list.begin();
		it != format_list.end();
		++it)
	{
		cout <<"channels: "<<(*it).channels<<", frequency: "<<(*it).frequency << endl;
	}

	input->GetFrameEvent()->Attach(this,&AudioInputTest::ProcessFrame);
	RETURN_IF_FALSE(input->Open((*format_list.begin())));
	Utils::Delay(1000);
	RETURN_IF_FALSE(input->Close());
	RETURN_IF_FALSE(_manager->Destroy(&input));
	return true;
}

void AudioInputTest::ProcessFrame(const Frame& frame)
{
	LOG_INFO("Received frame with pts "<<frame.pts<<" and size "<<frame.size);
}