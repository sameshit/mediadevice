#ifndef AUDIOINPUTTEST__H
#define AUDIOINPUTTEST__H

#include "../src/MediaDevice.h"

namespace MediaDeviceLib
{
	class AudioInputTest
	{
	public:
		AudioInputTest(CoreObjectLib::CoreObject *core);
		virtual ~AudioInputTest();

		bool Run();
	private:
		CoreObjectLib::CoreObject *_core;
		AudioInputManager *_manager;

		void ProcessFrame(const LiveFormatLib::Frame &frame);
	};
}

#endif