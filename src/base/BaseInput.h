#ifndef BASEINPUT_______HHH
#define BASEINPUT_______HHH

#include "../../../LiveFormat/src/LiveTypes.h"

namespace MediaDeviceLib
{
	typedef CoreObjectLib::Event<const LiveFormatLib::Frame&> FrameEvent;

	class BaseInput
	{
	public:
		BaseInput(CoreObjectLib::CoreObject *core);
		virtual ~BaseInput();

		virtual bool Close() = 0;
		virtual void* GetUserData() = 0;
		virtual void  SetUserData(void *udata) = 0;
		inline  void  SetSyncTime(const CoreObjectLib::COTime& sync_time) {_sync_time=sync_time;}
		
		FrameEvent OnFrame;
	protected:
		CoreObjectLib::CoreObject *_core;
		CoreObjectLib::Thread *_capture_thread;
		CoreObjectLib::COTime _sync_time;
		LiveFormatLib::Frame  _frame;
		bool _is_opened;

		void ProcessFrame();
		virtual void ProcessCapture() = 0;
	};
}

#endif