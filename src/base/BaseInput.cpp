#include "BaseInput.h"

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;
using namespace std;

BaseInput::BaseInput(CoreObject *core)
	:_core(core),_is_opened(false)
{
	_capture_thread = _core->GetThread();
	_capture_thread->OnThread.Attach(this,&BaseInput::ProcessCapture);
}

BaseInput::~BaseInput()
{
	_core->ReleaseThread(&_capture_thread);
}

void BaseInput::ProcessFrame()
{
	COTime now;

	_frame.pts = now-_sync_time;
	OnFrame(_frame);
}