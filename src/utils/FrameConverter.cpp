#include "FrameConverter.h"

using namespace MediaDeviceLib;
using namespace LiveFormatLib;
using namespace CoreObjectLib;
using namespace std;

FrameConverter::FrameConverter()
	:_type(FrameConverterType::None)
{

}

FrameConverter::~FrameConverter()
{

}

bool FrameConverter::Open(const PixelFormat& input_pixel_format,
						  const PixelFormat& output_pixel_format,
						  const uint32_t& w, const uint32_t &h)
{
	switch(input_pixel_format)
	{
	case PixelFormat::YUY2:
		switch(output_pixel_format)
		{
		case PixelFormat::I420p:
			_type = FrameConverterType::YUV2_TO_YUV420p;
			_w = w;
			_h = h;
			return true;
		break;
		default:
			COErr::Set() << "Unsupported pixel conversion";
			return false;
		break;
		}
	break;
	default:
		COErr::Set() << "Unsupported pixel conversion";
		return false;
	break;
	}
}

bool FrameConverter::Convert(const uint8_t *input, uint8_t *output)
{
	switch (_type)
	{
	case FrameConverterType::YUV2_TO_YUV420p:
		YUV2_TO_YUV420p(input,output);
	break;
	default:
		COErr::Set() << "Invalid converter type. Did you Open() ?";
		return false;
	break;
	}
	return true;
}

void FrameConverter::YUV2_TO_YUV420p(const uint8_t *input, uint8_t *output)
{

	for (auto i = 0; i != _w * _h; ++i)
    output[i] = input[2 * i];

	for (auto j = 0; j != _w * _h / 4; ++j)
	{
    output[_w * _h + j]       = ( input[_h*(j / _h/2    ) + 4 * j + 3]
                                + input[_h*(j / _h/2 + 1) + 4 * j + 3] ) / 2;

    output[_w * _h * 5/4 + j] = ( input[_h*(j / _h/2    ) + 4 * j + 1]
                                + input[_h*(j / _h/2 + 1) + 4 * j + 1] ) / 2;
	}
}