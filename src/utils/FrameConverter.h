#ifndef FRAMECONVERTER__HH
#define FRAMECONVERTER__HH

#include "../../../LiveFormat/src/LiveTypes.h"

namespace MediaDeviceLib
{
	enum class FrameConverterType
	{
		None,
		YUV2_TO_YUV420p,
	};

	class FrameConverter
	{
	public:
		FrameConverter();
		virtual ~FrameConverter();

		bool Open(const LiveFormatLib::PixelFormat &input_pixel_format,
				  const LiveFormatLib::PixelFormat &output_pixel_format,
				  const uint32_t &w,const uint32_t &h);
		bool Convert(const uint8_t *input,uint8_t *output);
	private:
		bool _is_opened;
		FrameConverterType _type;
		uint32_t _w,_h;

		void YUV2_TO_YUV420p(const uint8_t *input,uint8_t *output);
	};
}

#endif