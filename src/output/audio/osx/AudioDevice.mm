#include "../AudioDevice.h"

using namespace CoreObjectLib;
using namespace VideoPlayerLib;
using namespace DecoderLib;
using namespace CoreObjectLib;


AudioDevice::AudioDevice(CoreObject *core)
:_core(core),_opened(false)
{
    fast_new(_sem,AUDIO_DEVICE_BUFFER_COUNT);
}

AudioDevice::~AudioDevice()
{
    Close();
    fast_delete(_sem);
}

bool AudioDevice::Open(const AudioHeader &audio_settings)
{
    AudioStreamBasicDescription format;
    OSStatus err;
    int i;
    
    if (_opened)
    {
        COErr::Set("Audio device is already opened");
        return false;
    }
    
    _audio_settings = audio_settings;
    
    format.mSampleRate       = audio_settings.sample_rate;
    format.mFormatID         = kAudioFormatLinearPCM;
    format.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger ;//| kAudioFormatFlagIsPacked does it need?;
    format.mBitsPerChannel   = 16;
    format.mChannelsPerFrame = audio_settings.channels;
    format.mBytesPerFrame    = 2 * format.mChannelsPerFrame;
    format.mFramesPerPacket  = 1;
    format.mBytesPerPacket   = format.mBytesPerFrame * format.mFramesPerPacket;
    format.mReserved         = 0;
    
    
    err = AudioQueueNewOutput (&format, &AudioDevice::AudioCallback, this, NULL,NULL, 0, &_aq);
    if (err != noErr)
    {
        COErr::Set("Couldn't create audio queue",err);
        return false;
    }
    
    for (i = 0 ; i < AUDIO_DEVICE_BUFFER_COUNT ; i ++)
    {
        err = AudioQueueAllocateBuffer(_aq, 1024*format.mBytesPerPacket, &_buffers[i]);
        if (err != noErr)
        {
            COErr::Set("Couldn't create audio queue buffer",err);
            return false;
        }
        _buffers[i]->mUserData = NULL;
    }    
    
    err = AudioQueueStart(_aq, NULL);
    if (err != noErr)
    {
        COErr::Set("Couldn't start audio queue",err);
        return false;
    }
    
    _opened = true;
    return true;
}

bool AudioDevice::Close()
{
    OSStatus err;
    int i;
    
    if (!_opened)
    {
        COErr::Set("Audio device is not opened",1);
        return false;
    }
    
    err = AudioQueueStop(_aq, true);
    if (err != noErr)
    {
        COErr::Set("Couldn't stop audio queue",err);
        return false;
    }
    
    for (i = 0; i < AUDIO_DEVICE_BUFFER_COUNT ; i ++)
    {
        err = AudioQueueFreeBuffer(_aq, _buffers[i]);
        if (err != noErr)
        {
            COErr::Set("Couldn't free audio queue buffer",err);
            return false;
        }
    }    
    _opened = false;
    return true;
}

bool AudioDevice::PlaySilence(uint32_t ms_duration)
{
    Frame frame;
    bool ret_val;
    
    if (ms_duration == 0)
        return true;
    
    frame.size = ms_duration*2*_audio_settings.sample_rate*_audio_settings.channels/1000;
    fast_alloc(frame.data, frame.size);
    memset((void*)frame.data,0,frame.size);
    ret_val = Play(frame);
    fast_free(frame.data);
    return ret_val;
}
    

bool AudioDevice::Play(const Frame &frame)
{
    int i;
    bool found;
    uint8_t *buf_data;
    OSStatus err;
    
    if (!_opened)
    {
        COErr::Set("Audio device is not opened",1);
        return false;
    }

    COND_RETURN(!_sem->Wait());
    
    found = false;
    for (i = 0 ; i < AUDIO_DEVICE_BUFFER_COUNT; i ++)
    {
        if (_buffers[i]->mUserData == NULL)
        {
            found = true;
            break;
        }
    }

    int cnt = 0;
    int j;
    for (j = 0 ; j < AUDIO_DEVICE_BUFFER_COUNT; j ++)
        if (_buffers[i]->mUserData == NULL)
            ++cnt;

    if (!found)
    {
        COErr::Set("Internal audio device error: couldn't find usable buffer",2);
        return false;
    }
    
    buf_data = (uint8_t *)_buffers[i]->mAudioData;

    memcpy(buf_data,frame.data,frame.size);
    _buffers[i]->mAudioDataByteSize = (uint32_t)frame.size;
    _buffers[i]->mUserData = this;    
    
    err = AudioQueueEnqueueBuffer(_aq, _buffers[i], 0, NULL);    
    if (err != noErr)
    {
        COErr::Set("Couldn't enqueue audio buffer",err);
        return false;
    }    
    return true;
}

void AudioDevice::AudioCallback(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer)
{
    AudioDevice *audio_device = (AudioDevice *)inUserData;
    
    inBuffer->mUserData = NULL;
    audio_device->_sem->Post();
}

bool AudioDevice::GetVolume(uint16_t *volume)
{
    AudioQueueParameterValue val;
    OSStatus err;
    
    err = AudioQueueGetParameter(_aq, kAudioQueueParam_Volume, &val);
    if (err != noErr)
    {
        COErr::Set("Couldn't get audio volume parameter",err);
        return false;
    }
    
    *volume = (uint16_t)(65535.0f*val);
    return true;
}

bool AudioDevice::SetVolume(uint16_t volume)
{
    AudioQueueParameterValue val;
    OSStatus err;
    
    val = ((float)volume)/65535.0f;
    err = AudioQueueSetParameter(_aq, kAudioQueueParam_Volume, val);
    
    if (err != noErr)
    {
        COErr::Set("Couldn't set audio volume parameter",err);
        return false;
    }
    
    return true;
}