#import "FullScreenView.h"

@interface MainThreadHelper :NSObject
{
@private
    NSWindow                    **_offscreen_window;
    VideoLayer                  **_video_layer;
    FullScreenView              **_fs_view;
    CALayer                     *_layer;
    CoreObjectLib::VideoHeader     _header;
    DecoderLib::PixelFormat   _pf;
    CoreObjectLib::CoreObject   *_core;
}
- (id) initOffscreen:(NSWindow **)offscreen_window_  andVideoLayer:(VideoLayer **)video_layer_ andFSView:(FullScreenView **)fs_view_ andCALayer:(CALayer *)layer_ andVideoHeader:(const CoreObjectLib::VideoHeader &)header_ andPixelFormat:(const DecoderLib::PixelFormat &)pf_ andCoreObject:(CoreObjectLib::CoreObject *)core_;

- (void) helpInit;
- (void) helpFree;
@end
