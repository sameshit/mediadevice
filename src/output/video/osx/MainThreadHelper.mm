//
//  MainThreadHelper.mm
//  VideoPlayer
//
//  Created by Oleg on 13.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "MainThreadHelper.h"

@implementation MainThreadHelper

- (id) initOffscreen:(NSWindow **)offscreen_window_ andVideoLayer:(VideoLayer **)video_layer_ andFSView:(FullScreenView **)fs_view_ andCALayer:(CALayer *)layer_ andVideoHeader:(const CoreObjectLib::VideoHeader &)header_ andPixelFormat:(const DecoderLib::PixelFormat &)pf_ andCoreObject:(CoreObjectLib::CoreObject *)core_
{
    self->_offscreen_window = offscreen_window_;
    self->_video_layer = video_layer_;
    self->_fs_view = fs_view_;
    self->_layer  = layer_;
    self->_header = header_;
    self->_core = core_;
    self->_pf = pf_;
    
    return self;
}

- (void) helpInit
{
    NSRect bounds;
    
    bounds = [[NSScreen mainScreen] frame];
    
    *_offscreen_window = [[NSWindow alloc] initWithContentRect:bounds styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:YES];
    *_video_layer = [[VideoLayer alloc] initWithFrame:[_layer frame] andCoreObject:_core andVideoHeader:_header andPixelFormat:_pf];
    *_fs_view     = [[FullScreenView alloc] initWithFrame:bounds andCoreObject:_core andVideoHeader:_header andPixelFormat:_pf  andNotFsLayer:*_video_layer];
    
    
    [*_offscreen_window setContentView:*_fs_view];
    [_layer addSublayer:*_video_layer];
}

- (void) helpFree
{
    [*_offscreen_window  release];
    [*_video_layer myRelease];
    [*_video_layer       release];
    LOG "help free" EL;
    [*_fs_view myRelease];
    [*_fs_view   release];
    
    *_offscreen_window = nil;
    *_video_layer = nil;
    *_fs_view = nil;
}

@end