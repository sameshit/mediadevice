#import "FullScreenView.h"
#include "VideoLayer.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace DecoderLib;
using namespace PeerLib;

@implementation FullScreenView

- (id)initWithFrame:(NSRect)frame andCoreObject:(CoreObject*)core_ andVideoHeader:(const VideoHeader&)header_ andPixelFormat:(const PixelFormat &)pf_ andNotFsLayer:(VideoLayer*)not_fs_layer_;
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        _core           = core_;
        _header         = header_;
        _not_fs_layer   = not_fs_layer_;
        _fullscreen     = false;
        _pf             = pf_;
        
        _video_layer = [[VideoLayer alloc] initWithFrame:NSRectToCGRect(frame) andCoreObject:_core andVideoHeader:_header andPixelFormat:_pf];
        
        [self setWantsLayer:YES];                
        [self setLayer:_video_layer];              
    }
    
    return self;
}

- (void)myRelease
{
    [_video_layer myRelease];
    [_video_layer release];
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

- (void) mouseDown: (NSEvent *) event
{
    _fullscreen = false;
    [self exitFullScreenModeWithOptions:nil];    
}
- (void)goFullScreen
{
    _fullscreen = true;
    [self enterFullScreenMode:[NSScreen mainScreen] withOptions:nil];
}

- (bool)isFullScreen
{
    return _fullscreen;
}

- (void) Draw:(const Frame &)frame_
{
    [_video_layer Draw:frame_];
}
@end
