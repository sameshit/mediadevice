#ifndef VIDEOLAYER_H
#define VIDEOLAYER_H

#include "../../../Decoder/src/Decoder.h"
#include "../Renderer.h"

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>


@interface VideoLayer : CAOpenGLLayer 
{
@private
    VideoPlayerLib::Renderer                        *_renderer;
    DecoderLib::PixelFormat                         _pf;
    CoreObjectLib::CoreObject                       *_core;
    CoreObjectLib::VideoHeader                      _header;
    CoreObjectLib::SpinLock                         _lock;
    CoreObjectLib::Frame                            _frame;
}
- (id) initWithFrame:(CGRect)rect andCoreObject:(CoreObjectLib::CoreObject*)core_ andVideoHeader:(const CoreObjectLib::VideoHeader &)header_ andPixelFormat:(const DecoderLib::PixelFormat &)pf_;
- (void) Draw:(const CoreObjectLib::Frame &)frame_;
- (void) myRelease;
@end

#endif