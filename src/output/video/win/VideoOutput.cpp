#include "../VideoOutput.h"

using namespace MediaDeviceLib;
using namespace LiveFormatLib;
using namespace CoreObjectLib;
using namespace std;

const char* kWindowClassName = "JetPeer Player";

VideoOutput::VideoOutput(CoreObject *core)
	:_hrc(NULL),_core(core),_show_cursor(true),_fullscreen(false),_is_opened(false)
{
	fast_new(_renderer,_core);
	ABORT_MSG_IF_FALSE(CreateChildWindow(),"Couldn't create child window. Error: "<<COErr::Get());
}

VideoOutput::~VideoOutput()
{
	if (_is_opened)
		Close();
	
	DestroyWindow(_hwnd);
	UnregisterClass(TEXT(kWindowClassName),GetModuleHandle(NULL));
	fast_delete(_renderer);
}

bool VideoOutput::Open(const VideoHeader &header,const PixelFormat &pf, HWND hwnd)
{
    GLuint pixel_format;
	ImageSize window_size;
	RECT client_rect;

	RETURN_MSG_IF_TRUE(_is_opened,"Video device is already opened");
	_parent = hwnd;
	
	RETURN_MSG_IF_FALSE(GetClientRect(_parent,&client_rect)==TRUE,"GetClientRect failed: "<<GetLastError());
	
	_window_width = client_rect.right - client_rect.left;
	_window_height = client_rect.bottom - client_rect.top;

	_fullscreen = true;
	ToggleFullScreen();

    PIXELFORMATDESCRIPTOR pixel_format_descriptor = 
    {
        sizeof(PIXELFORMATDESCRIPTOR),                         
        1,                                                                                      
        PFD_DRAW_TO_WINDOW |                                           
        PFD_SUPPORT_OPENGL |                                          
        PFD_DOUBLEBUFFER,                                                      
        PFD_TYPE_RGBA,                                                         
        32,                                                                                    
        0, 0, 0, 0, 0, 0,                                                      
        0,                                                                     
        0,                                                                     
        0,                                                                     
        0, 0, 0, 0,                                                            
        32,                                                                  
        0,                                                                   
        0,                                                           
        PFD_MAIN_PLANE,                                                     
        0,                                                                                
        0, 0, 0                                                                   
    };
        
    RETURN_MSG_IF_FALSE(pixel_format = ChoosePixelFormat(_hdc, &pixel_format_descriptor),"Couldn't choose pixel format");         
    RETURN_MSG_IF_FALSE(SetPixelFormat(_hdc, pixel_format, &pixel_format_descriptor),"Couldn't set pixel format");
                
    RETURN_MSG_IF_FALSE(_hrc = wglCreateContext(_hdc),"Couldn't create wgl context. Error: "<<WinErrorUtils::GetLastErrorString());
	RETURN_MSG_IF_FALSE(wglMakeCurrent(_hdc, _hrc),"wglMakeCurrent failed. Error: "<<WinErrorUtils::GetLastErrorString());
        
	RETURN_MSG_IF_FALSE(glewInit() == GLEW_OK,"Couldn't init glew");
	RETURN_IF_FALSE(_renderer->Open(header,pf));

	window_size.width	= _window_width;
	window_size.height	= _window_height;

	RETURN_IF_FALSE(_renderer->Resize(window_size));

    RETURN_MSG_IF_FALSE(wglMakeCurrent(0, 0),"wglMakeCurrent2 failed. Error: "<<WinErrorUtils::GetLastErrorString());    

	_header = header;
	_is_opened = true;

	return true;
}

bool VideoOutput::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"VideoOutput is already closed");
	
    wglDeleteContext(_hrc);
	return true;
}

bool VideoOutput::Draw(Frame *frame)
{
	auto destructor = std::bind(&Renderer::FreeFrame,_renderer, std::placeholders::_1);
	std::unique_ptr<Frame, decltype(destructor)> px(frame, destructor);

	{
		ScopedSpin lock(_draw_lock);

		RETURN_MSG_IF_FALSE(wglMakeCurrent(_hdc, _hrc)==TRUE,"wglMakeCurrent error="<<WinErrorUtils::GetLastErrorString());
		RETURN_IF_FALSE(_renderer->Render(*frame));
		RETURN_MSG_IF_FALSE(SwapBuffers(_hdc)==TRUE,"SwapBuffer error="<<WinErrorUtils::GetLastErrorString());
		RETURN_MSG_IF_FALSE(wglMakeCurrent(0, 0)==TRUE,"wglMakeCurrent2 error="<<WinErrorUtils::GetLastErrorString());
	}

	return true;
}

bool VideoOutput::CreateChildWindow()
{
	WNDCLASS wnd_class = { sizeof(wnd_class) };
	stringstream hwnd_class_name;

	hwnd_class_name << kWindowClassName<<rand()<<rand();

	wnd_class.cbClsExtra = 0;
	wnd_class.cbWndExtra = 0;
    wnd_class.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wnd_class.hCursor = LoadCursor(0, IDC_ARROW);
    wnd_class.hIcon = LoadIcon(0, IDI_APPLICATION);
    wnd_class.hInstance = GetModuleHandle(NULL);
    wnd_class.lpfnWndProc = StaticWndProc;
	wnd_class.lpszClassName =hwnd_class_name.str().c_str();
    wnd_class.lpszMenuName = 0;
    wnd_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RETURN_MSG_IF_TRUE(RegisterClass(&wnd_class)==0,"Couldn't register class: "<<WinErrorUtils::GetLastErrorString());

	_hwnd = CreateWindow(TEXT(kWindowClassName), TEXT(""), WS_POPUP, 
            CW_USEDEFAULT, 0, 1, 1, NULL, 0, 0, 0);

	RETURN_MSG_IF_TRUE(_hwnd == NULL,"Create window failed. Error: "<<GetLastError());
	         
    SetWindowLongPtr(_hwnd,GWL_USERDATA,(LONG_PTR)this);
    UpdateWindow(_hwnd);

    _hdc  = GetDC(_hwnd);
	RETURN_MSG_IF_TRUE(_hdc == NULL,"GetDC error: "<<GetLastError());
                
    return true;
}

bool VideoOutput::ShowCursor()
{
        _show_cursor = true;
        SetCursor(LoadCursor(0, IDC_ARROW));
        return true;
}

bool VideoOutput::HideCursor()
{
        _show_cursor = false;
        SetCursor(NULL);
        return true;
}

LRESULT CALLBACK VideoOutput::StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
        VideoOutput *video_device;
		CoreObject *_core;
		
		video_device = (VideoOutput*)GetWindowLongPtr(hWnd,GWL_USERDATA);
		if (video_device != nullptr)
			_core = video_device->_core;
	
        switch(uMsg)
        {
        case WM_CLOSE:
                DestroyWindow(hWnd);
                break;
        case WM_DESTROY:
                break;
        case WM_SETCURSOR:
                {
                        return true;
                break;
                }
        case WM_KEYDOWN:
                {
                        switch(LOWORD(wParam))
                        {
                        case VK_ESCAPE:
                             
                                break;
                        case VK_F1:
                        break;
                        case VK_F2:
                        break;
                        default:
                        
                                break;
                        }
                        
                        break;
                }
        case WM_LBUTTONDOWN:
                {
					if (video_device != nullptr && _core != nullptr)
					{
                        if (!video_device->ToggleFullScreen())
							LOG_ERROR("Couldn't toggle fullscreen: "<<COErr::Get());
					}
                        break;
                }
        case WM_RBUTTONDOWN:
                {
                        break;
                }
        case WM_USER:
                {
                }
                break;
        default:
                break;
        }
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool VideoOutput::ToggleFullScreen()
{
	ImageSize wnd_size;

    if(!_fullscreen)
    {
		wnd_size.width  = GetSystemMetrics(SM_CXSCREEN);
		wnd_size.height = GetSystemMetrics(SM_CYSCREEN);

        SetParent(_hwnd, 0);

        SetWindowLongPtr(_hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);
        SetWindowLongPtr(_hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
		SetWindowPos(_hwnd, HWND_TOPMOST, 0, 0, wnd_size.width, wnd_size.height, SWP_SHOWWINDOW);
    }
    else
    {
		wnd_size.width  = _window_width;
		wnd_size.height = _window_height;
        SetWindowLongPtr(_hwnd, GWL_EXSTYLE, WS_EX_LEFT);
        SetWindowLongPtr(_hwnd, GWL_STYLE, WS_CHILD | WS_VISIBLE);
        SetWindowPos(_hwnd, HWND_NOTOPMOST,0, 0,wnd_size.width, wnd_size.height, SWP_SHOWWINDOW);

        SetParent(_hwnd, _parent);
    }

    _fullscreen = !_fullscreen;
	
	{
		ScopedSpin lock(_draw_lock);
	
		RETURN_MSG_IF_FALSE(wglMakeCurrent(_hdc, _hrc)==TRUE,"wglMakeCurrent error="<<WinErrorUtils::GetLastErrorString());
		RETURN_IF_FALSE(_renderer->Resize(wnd_size));
		RETURN_MSG_IF_FALSE(wglMakeCurrent(0, 0)==TRUE,"wglMakeCurrent2 error="<<WinErrorUtils::GetLastErrorString());
	}
    return true;
}