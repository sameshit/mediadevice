//
//  BaseRenderer.h
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __VideoPlayer__BaseRenderer__
#define __VideoPlayer__BaseRenderer__

#include "../../../../../LiveFormat/src/LiveTypes.h"

#if defined(OS_X)
#import <OpenGL/gl.h>
#elif defined(OS_WIN)
#define GLEW_STATIC
#include "../glew/glew.h"
#endif

namespace MediaDeviceLib
{
    typedef struct ImageSize
    {
        uint32_t width,height;
    }ImageSize;
    
    class BaseRenderer
    {
    protected:
        CoreObjectLib::CoreObject *_core;
    public:
        BaseRenderer(CoreObjectLib::CoreObject *core);
        virtual ~BaseRenderer();
        
        virtual bool Open   (const LiveFormatLib::VideoHeader &header,void *context_object = NULL) = 0;
        virtual bool Close  (void *context_object = NULL) = 0;
        virtual bool Clear  (void *context_object = NULL) = 0;
        virtual bool Render (const LiveFormatLib::Frame &frame,void *context_object = NULL) = 0;
        virtual bool Resize (const ImageSize &window_size, void *context_object = NULL) = 0;
        virtual void FreeFrame(LiveFormatLib::Frame *frame) = 0;
    };
}

#endif /* defined(__VideoPlayer__BaseRenderer__) */
