//
//  VDARenderer.h
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __VideoPlayer__VDARenderer__
#define __VideoPlayer__VDARenderer__

#include "../BaseRenderer.h"
#import <OpenGL/CGLIOSurface.h>
#import <CoreVideo/CoreVideo.h>

namespace VideoPlayerLib
{
    class VDARenderer
    :public BaseRenderer
    {
    private:
        bool                    _opened;
        CoreObjectLib::VideoHeader   _header;
        GLuint                  _texture;
        ImageSize               _window_size;
    public:
        VDARenderer(CoreObjectLib::CoreObject *core);
		virtual ~VDARenderer();
        
        bool Open       (const CoreObjectLib::VideoHeader &header,void *context_object);
        bool Close      (void *context_object);
        bool Clear      (void *context_object);
        bool Render     (const CoreObjectLib::Frame &frame,void *context_object);
        bool Resize     (const ImageSize &window_size,void *context_object);
        void FreeFrame  (CoreObjectLib::Frame *frame);
    };
}

#endif /* defined(__VideoPlayer__VDARenderer__) */
