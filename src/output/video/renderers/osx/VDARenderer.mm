//
//  VDARenderer.cpp
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "VDARenderer.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace PeerLib;

VDARenderer::VDARenderer(CoreObjectLib::CoreObject *core)
:BaseRenderer(core),_opened(false)
{

}

VDARenderer::~VDARenderer()
{
    
}

bool VDARenderer::Open(const VideoHeader &header,void *context_object)
{
    GLenum err;
    
    if (_opened)
    {
        COErr::Set("VDARenderer is already in opened state in Open()");
        return false;
    }
    
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
    glGenTextures(1, &_texture);
    glDisable(GL_TEXTURE_RECTANGLE_ARB);
    
    glClearColor(0.f, 0.f, 0.f, 1.f);
    
    if ((err =glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("VDARenderer failed to Open() with OpenGL error",err);
        return false;
    }
    
    _opened = true;
    _header = header;
    return true;
}

bool VDARenderer::Close(void *context_object)
{
    GLenum err;
    
    if (!_opened)
    {
        COErr::Set("VDARenderer is already in closed state in Close()");
        return false;
    }
    
    glDeleteTextures(1, &_texture);
    
    if ((err =glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("VDARenderer failed to Close() with OpenGL error",err);
        return false;
    }
    
    return true;
}

bool VDARenderer::Resize(const ImageSize &window_size, void *context_object)
{
	GLenum err;
    
    if (!_opened)
    {
        COErr::Set("YUV420Renderer is not in opened state in Resize()");
        return false;
    }
    
	_window_size = window_size;
    
	glViewport(0, 0, _window_size.width,_window_size.height);
    
	if ((err = glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("Couldn't set viewport",err);
        return false;
    }
    
	return true;
}

bool VDARenderer::Clear(void *context_object)
{
    if (!_opened)
    {
        COErr::Set("VDARenderer is not in opened state in Clear()");
        return false;
    }
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glFlush();
  
    return true;
}

bool VDARenderer::Render(const Frame &frame, void *context_object)
{
    uint32_t w,h;
    IOSurfaceRef io_surface;
    CVPixelBufferRef image;
    CGLContextObj cgl_ctx;
    GLfloat		texMatrix[16]	= {0};
    GLint		saveMatrixMode;
    GLenum err;
    
    if (!_opened)
    {
        COErr::Set("VDARenderer is not in opened state in Render()");
        return false;
    }
    
    cgl_ctx = (CGLContextObj)context_object;
    image = (CVPixelBufferRef)frame.data;
    io_surface = CVPixelBufferGetIOSurface(image);
    
    w	= (uint32_t)IOSurfaceGetWidth(io_surface);
    h	= (uint32_t)IOSurfaceGetHeight(io_surface);
    
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, _texture);
    CGLTexImageIOSurface2D(cgl_ctx, GL_TEXTURE_RECTANGLE_ARB, GL_RGB8,
                           w, h,
                           GL_YCBCR_422_APPLE, GL_UNSIGNED_SHORT_8_8_APPLE, io_surface, 0);
    
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
    glDisable(GL_TEXTURE_RECTANGLE_ARB);
        
    // Reverses and normalizes the texture
    
    float ratio;
    uint32_t new_width;
    uint32_t new_height;
    
    new_width	 = _window_size.width;
    new_height   = _window_size.width*_header.height/_header.width;
    
    ratio = (float)new_height/(float)_window_size.height;
    if (ratio < 1.f)
        ratio = 1/ratio;
    
    texMatrix[0]	= (GLfloat)w;
    texMatrix[5]	= -(GLfloat)h;
    texMatrix[10]	= 1.0;
    texMatrix[13]	= (GLfloat)h;
    texMatrix[15]	= 1.0;
    
    glGetIntegerv(GL_MATRIX_MODE, &saveMatrixMode);
    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadMatrixf(texMatrix);
    glMatrixMode(saveMatrixMode);
    
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, _texture);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, -1.0/ratio, 0.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f(1.0, -1.0/ratio, 0.0);
    glTexCoord2f(1.0, 1.0);
    glVertex3f(1.0, 1.0/ratio, 0.0);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-1.0, 1.0/ratio, 0.0);
    glEnd();
    
    glDisable(GL_TEXTURE_RECTANGLE_ARB);
    
    glGetIntegerv(GL_MATRIX_MODE, &saveMatrixMode);
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();
    glMatrixMode(saveMatrixMode);
        
    if ((err =glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("VDARenderer failed to Render() with OpenGL error",err);
        return false;
    }
    return true;
}

void VDARenderer::FreeFrame(Frame *frame)
{
    CVPixelBufferRef image;
    
    image = (CVPixelBufferRef)frame->data;
    CVPixelBufferRelease(image);
}