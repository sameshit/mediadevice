#ifndef MEDIADEVICE__H
#define MEDIADEVICE__H

#include "output/audio/AudioOutput.h"
#include "output/video/VideoOutput.h"
#include "input/video/VideoInputManager.h"
#include "input/audio/AudioInputManager.h"

#endif