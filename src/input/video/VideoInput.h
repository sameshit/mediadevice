#ifndef VIDEOINPUT__H
#define VIDEOINPUT__H

#include "devices/VideoDeviceInput.h"
#include "devices/ScreenInput.h"

namespace MediaDeviceLib
{
	class VideoInput
	{
	public:
		VideoInput(CoreObjectLib::CoreObject *core, const VideoInputInfo &info);
		virtual ~VideoInput();
		
		inline bool EnumFormats(VideoInputFormatList *list) {return _base_input->EnumFormats(list);}
		inline bool Open(const VideoInputFormat &format,const CoreObjectLib::Rational &framerate) 
						{return _base_input->Open(format,framerate);}
		inline bool Close() {return _base_input->Close();}

		inline const VideoInputInfo& GetInfo()	{return _info;}
		inline FrameEvent* GetFrameEvent() {return &_base_input->OnFrame;}
	private:
		CoreObjectLib::CoreObject *_core;
		BaseVideoInput *_base_input;
		VideoInputInfo _info;

		inline void SetUserData(void *udata) {_base_input->SetUserData(udata);}
		inline void*GetUserData()			 {return _base_input->GetUserData();}

		friend class VideoInputManager;

	};
}

#endif