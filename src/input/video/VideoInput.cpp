#include "VideoInput.h"

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace std;

VideoInput::VideoInput(CoreObject *core, const VideoInputInfo &info)
:_core(core),_info(info)
{
	switch(_info.GetType())
	{
	case VideoInputType::Device:
		typed_new(VideoDeviceInput,_base_input,_core); 
	break;
	case VideoInputType::Screen:
		typed_new(ScreenInput,_base_input,_core);
	break;
	default:
		FATAL_ERROR("Invalid VideoInputInfo type");
	break;
	}
}

VideoInput::~VideoInput()
{
	fast_delete(_base_input);
}