#ifndef VIDEOINPUTINFO__H
#define VIDEOINPUTINFO__H

#include "../../../base/BaseInput.h"

namespace MediaDeviceLib
{
	enum class VideoInputType
	{
		Device,
		Screen,
	};

	class VideoInputInfo
	{
	public:
		VideoInputInfo() {}
		virtual ~VideoInputInfo() {}

		inline const std::string& GetName()			{return _name;}
		inline const VideoInputType& GetType()		{return _type;}

		inline const std::string& GetName()	const		{return _name;}
		inline const VideoInputType& GetType()	const	{return _type;}
	private:
		std::string _name;
		VideoInputType _type;
		friend class VideoInputManager;
	};

	class VideoInputFormat
	{
	public:
		VideoInputFormat() {}
		virtual ~VideoInputFormat() {}

		LiveFormatLib::PixelFormat pixel_format;
		uint32_t width,height;
		CoreObjectLib::Rational min_fps,max_fps;

		bool operator== (const VideoInputFormat &other) const
		{
			return pixel_format == other.pixel_format && width == other.width && height == other.height
				&& min_fps == other.min_fps && max_fps == other.max_fps;
		}
	};
	typedef std::list<VideoInputFormat>							VideoInputFormatList;
	typedef VideoInputFormatList::iterator						VideoInputFormatListIterator;
}

#endif