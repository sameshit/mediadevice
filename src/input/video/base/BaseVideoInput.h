#ifndef BASEVIDEOINPUT__H
#define BASEVIDEOINPUT__H

#include "VideoInputTypes.h"

namespace MediaDeviceLib
{
	class BaseVideoInput
		:public BaseInput
	{
	public:
		BaseVideoInput(CoreObjectLib::CoreObject *core);
		virtual ~BaseVideoInput();

		virtual bool EnumFormats(VideoInputFormatList *list) = 0;
		virtual bool Open(const VideoInputFormat &format,const CoreObjectLib::Rational &framerate) = 0;
	protected:
	};
}

#endif