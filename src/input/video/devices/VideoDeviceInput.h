#ifndef VideoDeviceInput____H
#define VideoDeviceInput____H

#include "../base/BaseVideoInput.h"
#if defined(OS_WIN)
#include <Mfidl.h>
#include <Mfapi.h>
#include <Mfreadwrite.h>
#include <MFError.h>
#endif

namespace MediaDeviceLib
{
	class VideoDeviceInput
		:public BaseVideoInput
	{
	public:
		VideoDeviceInput(CoreObjectLib::CoreObject *core);
		virtual ~VideoDeviceInput();

		bool EnumFormats(VideoInputFormatList *list);
		bool Open(const VideoInputFormat &format,const CoreObjectLib::Rational &fps);
		bool Close();
		void SetUserData(void *udata);
		void*GetUserData();
	private:
		void ProcessCapture();

#ifdef OS_WIN
		IMFMediaSource *_source;
		IMFSourceReader *_reader;

		bool SetFormatAndFramerate(const VideoInputFormat &format, const CoreObjectLib::Rational &framerate);
		HRESULT ProcessSamples(IMFSourceReader *pReader);
#endif
	};
}

#endif