#include "ScreenInput.h"

using namespace LiveFormatLib;
using namespace CoreObjectLib;
using namespace MediaDeviceLib;
using namespace std;

ScreenInput::ScreenInput(CoreObject *core)
	:BaseVideoInput(core)
{

}

ScreenInput::~ScreenInput()
{
	if (_is_opened)
		Close();
}

bool ScreenInput::Open(const VideoInputFormat &format,const Rational &fps)
{
	VideoInputFormat screen_format;
	BITMAPINFO bmi;
	int bits_per_pixel;

	RETURN_MSG_IF_TRUE(_is_opened,"ScreenInput is already opened");

	_screen_hdc = GetDC(NULL);
	bits_per_pixel = GetDeviceCaps(_screen_hdc,BITSPIXEL);
	switch (bits_per_pixel)
	{
	case 16:
		screen_format.pixel_format = PixelFormat::DIB16;
	break;
	case 24:
		screen_format.pixel_format = PixelFormat::DIB24;
	break;
	case 32:
		screen_format.pixel_format = PixelFormat::DIB32;
	break;
	default:
		COErr::Set() << "Unsupported RGB type with bits per pixel: "<<bits_per_pixel;
		return false;
	break;
	}

	screen_format.width = GetDeviceCaps(_screen_hdc, HORZRES);
	screen_format.height = GetDeviceCaps(_screen_hdc, VERTRES);
	screen_format.max_fps	= Rational(60,1);
	screen_format.min_fps	= Rational(1,5);

	memset(&bmi, 0,sizeof(BITMAPINFOHEADER));
	bmi.bmiHeader.biSize	=sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth		= format.width;
	bmi.bmiHeader.biHeight		= -(int)format.height;
	bmi.bmiHeader.biPlanes		= 1;
	bmi.bmiHeader.biBitCount	= bits_per_pixel;
	bmi.bmiHeader.biCompression	= BI_RGB;

	RETURN_MSG_IF_FALSE(screen_format==format,"Provided format is not equal to screen's one");

	_my_hdc= CreateCompatibleDC (_screen_hdc);
	_hbitmap = CreateDIBSection(_my_hdc,&bmi,DIB_RGB_COLORS, (void**)&_frame.data, NULL, 0);
	_frame.size = ((((screen_format.width*bits_per_pixel)+31)>>5)<<2);
	SelectObject(_my_hdc, _hbitmap);
	_fps = fps;
	_format = format;
	RETURN_IF_FALSE(_capture_thread->Wake());
	_is_opened = true;
    
	return true;
}

bool ScreenInput::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"ScreenInput is already closed");
	RETURN_IF_FALSE(_capture_thread->Sleep());

	ReleaseDC(GetDesktopWindow(),_screen_hdc);
	DeleteDC(_my_hdc);
	_is_opened = false;
	return true;
}

bool ScreenInput::EnumFormats(VideoInputFormatList *list)
{
	uint32_t bits_per_pixel;
	HDC hdc; 
	VideoInputFormat format;
	
	hdc = GetDC(GetDesktopWindow());

	bits_per_pixel = GetDeviceCaps(hdc,BITSPIXEL);
	switch (bits_per_pixel)
	{
	case 16:
		format.pixel_format = PixelFormat::DIB16;
	break;
	case 24:
		format.pixel_format = PixelFormat::DIB24;
	break;
	case 32:
		format.pixel_format = PixelFormat::DIB32;
	break;
	default:
		COErr::Set() << "Unsupported RGB type with bits per pixel: "<<bits_per_pixel;
		return false;
	break;
	}

	format.max_fps = Rational(60,1);
	format.min_fps = Rational(1,5);

	format.width   = (uint32_t)GetDeviceCaps(hdc, HORZRES); 
	format.height  = (uint32_t)GetDeviceCaps(hdc, VERTRES);
	
	list->push_back(format);
	ReleaseDC(GetDesktopWindow(),hdc);
	return true;
}

void ScreenInput::ProcessCapture()
{
	COTime now;
	uint32_t delay;

	delay = 1000*_fps.denomerator/_fps.numerator;
	if (now-_prev_time <= delay)
	{
		delay -= now - _prev_time;
		Utils::Delay(delay);
	}
	_prev_time.Update();
	BitBlt(_my_hdc, 0, 0, _format.width, _format.height, (HDC)_screen_hdc, 0, 0, SRCCOPY);
	
	ProcessFrame();
}