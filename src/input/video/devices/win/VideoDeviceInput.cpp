#include "../VideoDeviceInput.h"
#include <Propvarutil.h>

using namespace LiveFormatLib;
using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace std;

VideoDeviceInput::VideoDeviceInput(CoreObject *core)
	:BaseVideoInput(core)
{
	MFStartup(MF_VERSION);
}

VideoDeviceInput::~VideoDeviceInput()
{
	if (_is_opened)
		Close();

	MFShutdown();
}

PixelFormat GuidToPixelFormat(const GUID &guid)
{
	if (guid == MFVideoFormat_YUY2)
		return PixelFormat::YUY2;
	else if (guid == MFVideoFormat_RGB32)
		return PixelFormat::RGB32;
	else if (guid == MFVideoFormat_RGB24)
		return PixelFormat::RGB24;
	else if (guid == MFVideoFormat_ARGB32)
		return PixelFormat::RGBA32;
	else if (guid == MFVideoFormat_I420)
		return PixelFormat::I420p;
	else
		return PixelFormat::Unknown;
}

bool VideoDeviceInput::EnumFormats(VideoInputFormatList *list)
{
	ComUniquePtr<IMFPresentationDescriptor> presentation_descriptor;
    ComUniquePtr<IMFStreamDescriptor> stream_descriptor;
    ComUniquePtr<IMFMediaTypeHandler> type_handler;
    ComUniquePtr<IMFMediaType> media_type;
	HRESULT hr;
	BOOL selected;
	DWORD types;
	PROPVARIANT var;
	VideoInputFormat input_format,prev_format;

	hr = _source->CreatePresentationDescriptor(presentation_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create presentation descriptor: "<<WinErrorUtils::HResultToString(hr));
    
	hr = presentation_descriptor->GetStreamDescriptorByIndex(0, &selected, stream_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get straem descriptor by index: "<<WinErrorUtils::HResultToString(hr));
    
	hr = stream_descriptor->GetMediaTypeHandler(type_handler.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type handler: "<<WinErrorUtils::HResultToString(hr));

    hr = type_handler->GetMediaTypeCount(&types);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type count: "<<WinErrorUtils::HResultToString(hr));

    for (DWORD i = 0; i < types; i++)
    {
		hr = type_handler->GetMediaTypeByIndex(i, media_type.Receive());
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type by index: "<<WinErrorUtils::HResultToString(hr));
		
		hr = media_type->GetItem(MF_MT_SUBTYPE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));
		
		input_format.pixel_format = GuidToPixelFormat(*var.puuid);
		PropVariantClear(&var);
		if (input_format.pixel_format == PixelFormat::Unknown)
			continue;
	
		hr = media_type->GetItem(MF_MT_FRAME_SIZE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));

		input_format.width = var.uhVal.HighPart;
		input_format.height = var.uhVal.LowPart;

		hr = media_type->GetItem(MF_MT_FRAME_RATE_RANGE_MAX,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get max frame rate: "<<WinErrorUtils::HResultToString(hr));

		input_format.max_fps = Rational(var.uhVal.HighPart,var.uhVal.LowPart);

		hr = media_type->GetItem(MF_MT_FRAME_RATE_RANGE_MIN,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get max frame rate: "<<WinErrorUtils::HResultToString(hr));

		input_format.min_fps = Rational(var.uhVal.HighPart,var.uhVal.LowPart);
		if (i == 0 || !(prev_format == input_format))
			list->push_back(input_format);
		prev_format = input_format;
    }

	return true;
}

bool VideoDeviceInput::SetFormatAndFramerate(const VideoInputFormat &format, const Rational &fps)
{
	ComUniquePtr<IMFPresentationDescriptor> presentation_descriptor;
    ComUniquePtr<IMFStreamDescriptor> stream_descriptor;
    ComUniquePtr<IMFMediaTypeHandler> type_handler;
    ComUniquePtr<IMFMediaType> media_type;
	HRESULT hr;
	BOOL selected;
	DWORD types;
	PROPVARIANT var;
	VideoInputFormat input_format;

	hr = _source->CreatePresentationDescriptor(presentation_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create presentation descriptor: "<<WinErrorUtils::HResultToString(hr));
    
	hr = presentation_descriptor->GetStreamDescriptorByIndex(0, &selected, stream_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get straem descriptor by index: "<<WinErrorUtils::HResultToString(hr));
    
	hr = stream_descriptor->GetMediaTypeHandler(type_handler.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type handler: "<<WinErrorUtils::HResultToString(hr));

    hr = type_handler->GetMediaTypeCount(&types);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type count: "<<WinErrorUtils::HResultToString(hr));

    for (DWORD i = 0; i < types; i++)
    {
		hr = type_handler->GetMediaTypeByIndex(i, media_type.Receive());
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type by index: "<<WinErrorUtils::HResultToString(hr));
		
		hr = media_type->GetItem(MF_MT_SUBTYPE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));
		
		input_format.pixel_format = GuidToPixelFormat(*var.puuid);
		PropVariantClear(&var);
		if (input_format.pixel_format == PixelFormat::Unknown)
			continue;
	
		hr = media_type->GetItem(MF_MT_FRAME_SIZE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));

		input_format.width = var.uhVal.HighPart;
		input_format.height = var.uhVal.LowPart;
		PropVariantClear(&var);

		hr = media_type->GetItem(MF_MT_FRAME_RATE_RANGE_MAX,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get max frame rate: "<<WinErrorUtils::HResultToString(hr));

		input_format.max_fps = Rational(var.uhVal.HighPart,var.uhVal.LowPart);
		PropVariantClear(&var);

		hr = media_type->GetItem(MF_MT_FRAME_RATE_RANGE_MIN,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get max frame rate: "<<WinErrorUtils::HResultToString(hr));

		input_format.min_fps = Rational(var.uhVal.HighPart,var.uhVal.LowPart);
		PropVariantClear(&var);

		if (input_format == format)
		{
			uint64_t framerate_val;
			uint8_t *pos;

			pos = (uint8_t*)&framerate_val;
			*(uint32_t*)pos = fps.denomerator;
			pos += 4;
			*(uint32_t*)pos = fps.numerator;

			InitPropVariantFromUInt64(framerate_val,&var);

			hr = media_type->SetItem(MF_MT_FRAME_RATE,var);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set frame rate: "<<WinErrorUtils::HResultToString(hr));
			
			hr = type_handler->SetCurrentMediaType(media_type.Get());
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set current media type: "<<WinErrorUtils::HResultToString(hr));
			return true;
		}
    }

	COErr::Set() << "Couldn't find such format";
	return false;
}

bool VideoDeviceInput::Open(const VideoInputFormat &format,const Rational& fps)
{
	HRESULT hr;

	RETURN_MSG_IF_TRUE(_is_opened,"VideoDeviceInput is already opened");	
	RETURN_IF_FALSE(SetFormatAndFramerate(format,fps));

	hr = MFCreateSourceReaderFromMediaSource(_source,nullptr,&_reader);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create source reader: "<<WinErrorUtils::HResultToString(hr));
	RETURN_IF_FALSE(_capture_thread->Wake());

	_is_opened = true;
	return true;
}

bool VideoDeviceInput::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"VideoDeviceInput is already closed");

	RETURN_IF_FALSE(_capture_thread->Sleep());
	_source->Release();
	_reader->Release();
	_is_opened = false;

	return true;
}

void VideoDeviceInput::ProcessCapture()
{
	IMFSample *sample;
	HRESULT hr;
	DWORD flags,maxlen,curlen;
	IMFMediaBuffer *buffer;
	uint8_t *ubuf;

	sample = nullptr;

	hr = _reader->ReadSample(
    DWORD(MF_SOURCE_READER_FIRST_VIDEO_STREAM),    // Stream index.
    0,                              // Flags.
	NULL,                   // Receives the actual stream index. 
    &flags,                         // Receives status flags.
    NULL,                   // Receives the time stamp.
    &sample                        // Receives the sample or NULL.
    );

	ABORT_MSG_IF_TRUE(FAILED(hr),"ReadSample error: "<<WinErrorUtils::HResultToString(hr)<<", flags: "<<flags);

	if (sample != nullptr)
	{
		hr = sample->GetBufferByIndex(0,&buffer);
		ABORT_MSG_IF_TRUE(FAILED(hr),"GetBufferByIndex error: "<<WinErrorUtils::HResultToString(hr));

		hr = buffer->Lock(&ubuf,&maxlen,&curlen);
		ABORT_MSG_IF_TRUE(FAILED(hr),"Lock error: "<<WinErrorUtils::HResultToString(hr));

		_frame.size = curlen;
		_frame.data = ubuf;
		ProcessFrame();

		hr = buffer->Unlock();
		ABORT_MSG_IF_TRUE(FAILED(hr),"Unlock error: "<<WinErrorUtils::HResultToString(hr));
		buffer->Release();
		sample->Release();
	}
}

void VideoDeviceInput::SetUserData(void *udata)
{
	_source = (IMFMediaSource *)udata;
}

void* VideoDeviceInput::GetUserData()
{
	return (void*)&_source;
}