#ifndef SCREENINPUT__HH
#define SCREENINPUT__HH

#include "../base/BaseVideoInput.h"

namespace MediaDeviceLib
{
	class ScreenInput
		:public BaseVideoInput
	{
	public:
		ScreenInput(CoreObjectLib::CoreObject *core);
		virtual ~ScreenInput();

		bool EnumFormats(VideoInputFormatList *list);
		bool Open(const VideoInputFormat &format,const CoreObjectLib::Rational &fps);
		bool Close();
		inline void* GetUserData() {return nullptr;};
		inline void  SetUserData(void *udata) {};
	private:
		CoreObjectLib::Rational _fps;

		void ProcessCapture();
#if defined(OS_WIN)
		HDC _screen_hdc,_my_hdc;
		HBITMAP _hbitmap;
		VideoInputFormat _format;
		CoreObjectLib::COTime _prev_time;
#endif
	};
}

#endif