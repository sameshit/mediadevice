#include "../VideoInputManager.h"
#include <Mfidl.h>
#include <Mfapi.h>
#include <Dbt.h>

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace std;

VideoInputManager::VideoInputManager(CoreObject *core)
	:_core(core)
{
	CoInitialize(NULL);
}

VideoInputManager::~VideoInputManager()
{
	ABORT_MSG_IF_FALSE(_input_set.empty(),"Not all("<<_input_set.size()<<") input devices were unregistered");
	CoUninitialize();
}

bool VideoInputManager::Enum(VideoInputInfoList *info_list)
{
	ComUniquePtr<IMFAttributes> attributes;
	IMFActivate** devices; 
	UINT32 count;
	HRESULT hr;
	VideoInputInfo video_info;

	video_info._name = "Screen";
	video_info._type = VideoInputType::Screen;
	info_list->push_back(video_info);

	hr = MFCreateAttributes(attributes.Receive(), 1);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create attributes: "<<WinErrorUtils::HResultToString(hr));

    hr = attributes->SetGUID(
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, 
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
        );
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set GUID: "<<WinErrorUtils::HResultToString(hr));
    
	hr = MFEnumDeviceSources(attributes.Get(), &devices, &count);
	RETURN_MSG_IF_TRUE(FAILED(hr), "Couldn't enum devices: "<<WinErrorUtils::HResultToString(hr));
  
	for (UINT32 i = 0; i < count; ++i)
	{
		LPWSTR name;
		UINT32 size;
		
		hr = devices[i]->GetAllocatedString(MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,&name,&size);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get allocated string: "<<WinErrorUtils::HResultToString(hr));
		
		video_info._name.assign(StringConvert::Utf16ToUtf8(wstring(name,size)));
		video_info._type = VideoInputType::Device;
		info_list->push_back(video_info);
		devices[i]->Release();
		CoTaskMemFree(name);
	}
	CoTaskMemFree(devices);

	return true;
}

bool VideoInputManager::Create(VideoInput **pvideo_input,const VideoInputInfo &info)
{
	ComUniquePtr<IMFAttributes> attributes;
	IMFActivate** devices; 
	UINT32 count;
	HRESULT hr;
	VideoInputInfo video_info;
	VideoInput *video_input;
	string str_name;

	if (info._type==VideoInputType::Screen)
	{
		fast_new(video_input,_core,info);
		*pvideo_input = video_input;
		_input_set.insert(video_input);
		return true;
	}

	hr = MFCreateAttributes(attributes.Receive(), 1);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create attributes: "<<WinErrorUtils::HResultToString(hr));

    hr = attributes->SetGUID(
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, 
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
        );
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set GUID: "<<WinErrorUtils::HResultToString(hr));
    
	hr = MFEnumDeviceSources(attributes.Get(), &devices, &count);
	RETURN_MSG_IF_TRUE(FAILED(hr), "Couldn't enum devices: "<<WinErrorUtils::HResultToString(hr));
  
	video_input = nullptr;
	for (UINT32 i = 0; i < count; ++i)
	{
		LPWSTR name;
		UINT32 size;
		
		if (video_input == nullptr)
		{
			hr = devices[i]->GetAllocatedString(MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,&name,&size);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get allocated string: "<<WinErrorUtils::HResultToString(hr));
		
			str_name.assign(StringConvert::Utf16ToUtf8(wstring(name,size)));
			if (str_name == info._name)
			{
				fast_new(video_input,_core,info);
				hr = devices[i]->ActivateObject(IID_PPV_ARGS((IMFMediaSource**)video_input->GetUserData()));
				RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't activate object: "<<WinErrorUtils::HResultToString(hr));
				_input_set.insert(video_input);
			}
		}

		devices[i]->Release();
		CoTaskMemFree(name);
	}
	CoTaskMemFree(devices);

	if (video_input == nullptr)
	{
		COErr::Set() << "Couldn't find suitable format";
		return false;
	}
	else
	{
		*pvideo_input = video_input;
		return true;
	}	
}

bool VideoInputManager::Destroy(VideoInput **pvideo_input)
{
	VideoInput *video_input;

	video_input = *pvideo_input;
	RETURN_MSG_IF_TRUE(_input_set.count(video_input) == 0,"Trying to destroy unregistered capture device");

	_input_set.erase(video_input);
	fast_delete(video_input);
	pvideo_input = nullptr;
	return true;
}