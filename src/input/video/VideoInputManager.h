#ifndef VIDEOINPUTMANAGER___H
#define VIDEOINPUTMANAGER___H

#include "VideoInput.h"

namespace MediaDeviceLib
{
	typedef std::list<VideoInputInfo> VideoInputInfoList;
	typedef std::unordered_set<VideoInput *> VideoInputSet;
	typedef VideoInputSet::iterator	VideoInputSetIterator;

	class VideoInputManager
	{
	public:
		VideoInputManager(CoreObjectLib::CoreObject *core);
		virtual ~VideoInputManager();

		bool Enum(VideoInputInfoList *input_set);
		bool Create(VideoInput **video_input,const VideoInputInfo &info);
		bool Destroy(VideoInput **video_input);
	private:
		CoreObjectLib::CoreObject *_core;
		VideoInputSet _input_set;
	};
}

#endif