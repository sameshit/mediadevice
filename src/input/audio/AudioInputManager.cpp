#include "AudioInputManager.h"
#include <Mfidl.h>
#include <Mfapi.h>
#include <Dbt.h>


using namespace CoreObjectLib;
using namespace MediaDeviceLib;
using namespace std;
using namespace LiveFormatLib;

AudioInputManager::AudioInputManager(CoreObject *core)
	:_core(core)
{
	CoInitialize(NULL);
}

AudioInputManager::~AudioInputManager()
{
	CoUninitialize();
	ABORT_MSG_IF_FALSE(_input_set.empty(),"Audio input set is not empty! Did you forget to call Destroy() ?");
}

bool AudioInputManager::Enum(AudioInputInfoList *info_list)
{
	ComUniquePtr<IMFAttributes> attributes;
	IMFActivate** devices; 
	UINT32 count;
	HRESULT hr;
	AudioInputInfo audio_info;

	hr = MFCreateAttributes(attributes.Receive(), 1);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create attributes: "<<WinErrorUtils::HResultToString(hr));

    hr = attributes->SetGUID(
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, 
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_AUDCAP_GUID
        );
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set GUID: "<<WinErrorUtils::HResultToString(hr));
    
	hr = MFEnumDeviceSources(attributes.Get(), &devices, &count);
	RETURN_MSG_IF_TRUE(FAILED(hr), "Couldn't enum devices: "<<WinErrorUtils::HResultToString(hr));
  
	for (UINT32 i = 0; i < count; ++i)
	{
		LPWSTR name;
		UINT32 size;
		
		hr = devices[i]->GetAllocatedString(MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,&name,&size);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get allocated string: "<<WinErrorUtils::HResultToString(hr));
		
		audio_info._name.assign(StringConvert::Utf16ToUtf8(wstring(name,size)));
		info_list->push_back(audio_info);
		devices[i]->Release();
		CoTaskMemFree(name);
	}
	CoTaskMemFree(devices);

	return true;
}

bool AudioInputManager::Create(AudioInput **paudio_input,const AudioInputInfo& info)
{
	ComUniquePtr<IMFAttributes> attributes;
	IMFActivate** devices; 
	UINT32 count;
	HRESULT hr;
	AudioInputInfo video_info;
	AudioInput *audio_input;
	string str_name;

	hr = MFCreateAttributes(attributes.Receive(), 1);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create attributes: "<<WinErrorUtils::HResultToString(hr));

    hr = attributes->SetGUID(
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, 
        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_AUDCAP_GUID
        );
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set GUID: "<<WinErrorUtils::HResultToString(hr));
    
	hr = MFEnumDeviceSources(attributes.Get(), &devices, &count);
	RETURN_MSG_IF_TRUE(FAILED(hr), "Couldn't enum devices: "<<WinErrorUtils::HResultToString(hr));
  
	audio_input = nullptr;
	for (UINT32 i = 0; i < count; ++i)
	{
		LPWSTR name;
		UINT32 size;
		
		if (audio_input == nullptr)
		{
			hr = devices[i]->GetAllocatedString(MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,&name,&size);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get allocated string: "<<WinErrorUtils::HResultToString(hr));
		
			str_name.assign(StringConvert::Utf16ToUtf8(wstring(name,size)));
			if (str_name == info._name)
			{
				fast_new(audio_input,_core,info);
				hr = devices[i]->ActivateObject(IID_PPV_ARGS((IMFMediaSource**)audio_input->GetUserData()));
				RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't activate object: "<<WinErrorUtils::HResultToString(hr));
				_input_set.insert(audio_input);
			}
		}

		devices[i]->Release();
		CoTaskMemFree(name);
	}
	CoTaskMemFree(devices);

	if (audio_input == nullptr)
	{
		COErr::Set() << "Couldn't find suitable format";
		return false;
	}
	else
	{
		*paudio_input = audio_input;
		return true;
	}	
}

bool AudioInputManager::Destroy(AudioInput **paudio_input)
{
	AudioInput *audio_input;

	audio_input = *paudio_input;
	RETURN_MSG_IF_TRUE(_input_set.count(audio_input) == 0,"Trying to destroy unregistered capture device");

	_input_set.erase(audio_input);
	fast_delete(audio_input);
	paudio_input = nullptr;
	return true;
}