#ifndef AUDIOINPUTMANAGER__H
#define AUDIOINPUTMANAGER__H

#include "AudioInput.h"

namespace MediaDeviceLib
{
	typedef std::list<AudioInputInfo> AudioInputInfoList;
	typedef AudioInputInfoList::iterator AudioInputInfoListIterator;
	typedef std::unordered_set<AudioInput *> AudioInputSet;
	typedef AudioInputSet::iterator AudioInputSetIterator;

	class AudioInputManager
	{
	public:
		AudioInputManager(CoreObjectLib::CoreObject *core);
		virtual ~AudioInputManager();

		bool Enum(AudioInputInfoList *list);
		bool Create(AudioInput **audio_input, const AudioInputInfo &info);
		bool Destroy(AudioInput **audio_input);
	private:
		CoreObjectLib::CoreObject *_core;
		AudioInputSet _input_set;
	};
}

#endif