#ifndef AUDIOINPUT___H
#define AUDIOINPUT___H

#include "../../base/BaseInput.h"
#if defined(OS_WIN)
#include <Mfidl.h>
#include <Mfapi.h>
#include <Mfreadwrite.h>
#include <MFError.h>
#endif

namespace MediaDeviceLib
{
	class AudioInputInfo
	{
	public:
		AudioInputInfo() {}
		virtual ~AudioInputInfo() {}

		inline const std::string& GetName()				{return _name;}
		inline const std::string& GetName()	const		{return _name;}
	private:
		std::string _name;
		friend class AudioInputManager;
	};

	class AudioInputFormat
	{
	public:
		AudioInputFormat() {}
		virtual ~AudioInputFormat() {}
		
		uint32_t					channels;
		uint32_t					frequency;
		LiveFormatLib::AudioFormat	format;

		bool operator == (const AudioInputFormat &other) const
		{
			return channels==other.channels && frequency==other.frequency
				&& format==other.format;
		}
	};

	typedef std::list<AudioInputFormat> AudioInputFormatList;
	typedef AudioInputFormatList::iterator AudioInputFormatListIterator;

	class AudioInput
		:public BaseInput
	{
	public:
		AudioInput(CoreObjectLib::CoreObject *core,const AudioInputInfo &info);
		virtual ~AudioInput();

		bool EnumFormats(AudioInputFormatList *list);
		bool Open(const AudioInputFormat &format);
		bool Close();
		
		const AudioInputInfo& GetInfo() {return _info;}
		inline FrameEvent* GetFrameEvent() {return &OnFrame;}
	private:
		void SetUserData(void *udata);
		void*GetUserData();
		void ProcessCapture();
		AudioInputInfo _info;
#if defined(OS_WIN)
		IMFMediaSource *_source;
		IMFSourceReader *_reader;

		bool FindFormat(const AudioInputFormat &format);
#endif
		friend class AudioInputManager;
	};
}

#endif