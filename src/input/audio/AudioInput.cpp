#include "AudioInput.h"

using namespace MediaDeviceLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;

AudioInput::AudioInput(CoreObject *core, const AudioInputInfo &info)
	:BaseInput(core),_info(info)
{
	MFStartup(MF_VERSION);
}

AudioInput::~AudioInput()
{
	if (_is_opened)
		Close();
	MFShutdown();
}

AudioFormat GUIDToAudioFormat(const GUID& guid)
{
	if (guid == MFAudioFormat_PCM)
		return AudioFormat::PCM;
	else if (guid == MFAudioFormat_Float)
		return AudioFormat::Float;
	else
		return AudioFormat::Unknown;
}

bool AudioInput::EnumFormats(AudioInputFormatList *list)
{
	ComUniquePtr<IMFPresentationDescriptor> presentation_descriptor;
    ComUniquePtr<IMFStreamDescriptor> stream_descriptor;
    ComUniquePtr<IMFMediaTypeHandler> type_handler;
    ComUniquePtr<IMFMediaType> media_type;
	HRESULT hr;
	BOOL selected;
	DWORD types;
	PROPVARIANT var;
	AudioInputFormat input_format;

	hr = _source->CreatePresentationDescriptor(presentation_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create presentation descriptor: "<<WinErrorUtils::HResultToString(hr));
    
	hr = presentation_descriptor->GetStreamDescriptorByIndex(0, &selected, stream_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get straem descriptor by index: "<<WinErrorUtils::HResultToString(hr));
    
	hr = stream_descriptor->GetMediaTypeHandler(type_handler.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type handler: "<<WinErrorUtils::HResultToString(hr));

    hr = type_handler->GetMediaTypeCount(&types);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type count: "<<WinErrorUtils::HResultToString(hr));

    for (DWORD i = 0; i < types; i++)
    {
		hr = type_handler->GetMediaTypeByIndex(i, media_type.Receive());
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type by index: "<<WinErrorUtils::HResultToString(hr));
		
		hr = media_type->GetItem(MF_MT_SUBTYPE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));
		
		input_format.format = GUIDToAudioFormat(*var.puuid);
		PropVariantClear(&var);

		if (input_format.format != AudioFormat::Unknown)
		{
			hr = media_type->GetItem(MF_MT_AUDIO_NUM_CHANNELS,&var);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get num channels: "<<WinErrorUtils::HResultToString(hr));
			
			input_format.channels = var.ulVal;
			PropVariantClear(&var);

			hr = media_type->GetItem(MF_MT_AUDIO_SAMPLES_PER_SECOND,&var);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get samples per second: "<<WinErrorUtils::HResultToString(hr));

			input_format.frequency = var.ulVal;
			PropVariantClear(&var);
			list->push_back(input_format);
		}
    }

	return true;
}

bool AudioInput::Open(const AudioInputFormat &format)
{
	HRESULT hr;

	RETURN_MSG_IF_TRUE(_is_opened,"AudioDeviceInput is already opened");	
	RETURN_IF_FALSE(FindFormat(format));

	hr = MFCreateSourceReaderFromMediaSource(_source,nullptr,&_reader);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create source reader: "<<WinErrorUtils::HResultToString(hr));
	RETURN_IF_FALSE(_capture_thread->Wake());

	_is_opened = true;
	return true;
}

bool AudioInput::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"VideoDeviceInput is already closed");

	RETURN_IF_FALSE(_capture_thread->Sleep());
	_source->Release();
	_reader->Release();
	_is_opened = false;

	return true;
}

void AudioInput::SetUserData(void *udata)
{
	_source = (IMFMediaSource *)udata;
}

void* AudioInput::GetUserData()
{
	return (void*)&_source;
}

void AudioInput::ProcessCapture()
{
	IMFSample *sample;
	HRESULT hr;
	DWORD flags,maxlen,curlen;
	IMFMediaBuffer *buffer;
	uint8_t *ubuf;

	sample = nullptr;

	hr = _reader->ReadSample(
    DWORD(MF_SOURCE_READER_FIRST_AUDIO_STREAM),    // Stream index.
    0,                              // Flags.
	NULL,                   // Receives the actual stream index. 
    &flags,                         // Receives status flags.
    NULL,                   // Receives the time stamp.
    &sample                        // Receives the sample or NULL.
    );

	ABORT_MSG_IF_TRUE(FAILED(hr),"ReadSample error: "<<WinErrorUtils::HResultToString(hr)<<", flags: "<<flags);

	if (sample != nullptr)
	{
		hr = sample->GetBufferByIndex(0,&buffer);
		ABORT_MSG_IF_TRUE(FAILED(hr),"GetBufferByIndex error: "<<WinErrorUtils::HResultToString(hr));

		hr = buffer->Lock(&ubuf,&maxlen,&curlen);
		ABORT_MSG_IF_TRUE(FAILED(hr),"Lock error: "<<WinErrorUtils::HResultToString(hr));

		_frame.size = curlen;
		_frame.data = ubuf;
		ProcessFrame();

		buffer->Unlock();
		ABORT_MSG_IF_TRUE(FAILED(hr),"Unlock error: "<<WinErrorUtils::HResultToString(hr));
		buffer->Release();
		sample->Release();
	}
}

bool AudioInput::FindFormat(const AudioInputFormat &format)
{
	ComUniquePtr<IMFPresentationDescriptor> presentation_descriptor;
    ComUniquePtr<IMFStreamDescriptor> stream_descriptor;
    ComUniquePtr<IMFMediaTypeHandler> type_handler;
    ComUniquePtr<IMFMediaType> media_type;
	HRESULT hr;
	BOOL selected;
	DWORD types;
	PROPVARIANT var;
	AudioInputFormat input_format;

	hr = _source->CreatePresentationDescriptor(presentation_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create presentation descriptor: "<<WinErrorUtils::HResultToString(hr));
    
	hr = presentation_descriptor->GetStreamDescriptorByIndex(0, &selected, stream_descriptor.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get straem descriptor by index: "<<WinErrorUtils::HResultToString(hr));
    
	hr = stream_descriptor->GetMediaTypeHandler(type_handler.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type handler: "<<WinErrorUtils::HResultToString(hr));

    hr = type_handler->GetMediaTypeCount(&types);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type count: "<<WinErrorUtils::HResultToString(hr));

    for (DWORD i = 0; i < types; i++)
    {
		hr = type_handler->GetMediaTypeByIndex(i, media_type.Receive());
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get media type by index: "<<WinErrorUtils::HResultToString(hr));
		
		hr = media_type->GetItem(MF_MT_SUBTYPE,&var);
		RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get subtype: "<<WinErrorUtils::HResultToString(hr));
		
		input_format.format = GUIDToAudioFormat(*var.puuid);
		PropVariantClear(&var);

		if (input_format.format != AudioFormat::Unknown)
		{
			hr = media_type->GetItem(MF_MT_AUDIO_NUM_CHANNELS,&var);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get num channels: "<<WinErrorUtils::HResultToString(hr));
			
			input_format.channels = var.ulVal;
			PropVariantClear(&var);

			hr = media_type->GetItem(MF_MT_AUDIO_SAMPLES_PER_SECOND,&var);
			RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't get samples per second: "<<WinErrorUtils::HResultToString(hr));

			input_format.frequency = var.ulVal;
			PropVariantClear(&var);

			if (input_format == format)
			{
				hr = type_handler->SetCurrentMediaType(media_type.Get());
				RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set current media type: "<<WinErrorUtils::HResultToString(hr));
				return true;
			}
		}
		
    }

	COErr::Set() << "Couldn' find provided format";
	return false;
}